<?php

namespace Mush\Status\Enum;

class StatusEnum
{
    public const CHARGE = 'charge';
    public const ATTEMPT = 'attempt';
}
