<?php

namespace Mush\Item\Enum;

class ReachEnum
{
    public const INVENTORY = 'inventory';
    public const SHELVE = 'shelve';
    public const SHELVE_NOT_HIDDEN = 'shelve';

    //@TODO not implemented because no use as of now
    //public const SHELVE_ONLY = 'shelve_only';
    //public const SHELVE_ONLY_NOT_HIDDEN = 'shelve_only_only_not_hidden';
}
