<?php

namespace Mush\Item\Enum;

class EquipmentEnum
{
    public const PATROL_SHIP_ALPHA_LONGANE = 'patrol_ship_alpha_longane';
    public const PATROL_SHIP_ALPHA_JUJUBE = 'patrol_ship_alpha_jujube';
    public const PATROL_SHIP_ALPHA_TAMARIN = 'patrol_ship_alpha_tamarin';
    public const PATROL_SHIP_BRAVO_SOCRATE = 'patrol_ship_alpha_socrate';
    public const PATROL_SHIP_BRAVO_EPICURE = 'patrol_ship_alpha_epicure';
    public const PATROL_SHIP_BRAVO_PLANTON = 'patrol_ship_alpha_planton';
    public const PATROL_SHIP_ALPHA_2_WALLIS = 'patrol_ship_alpha_2_wallis';
    public const PASIPHAE = 'pasiphae';
    public const ICARUS = 'icarus';
    public const ANTENNA = 'antenna';
    public const CALCULATOR = 'calculator';
    public const CAMERA = 'camera';
    public const COMMUNICATION_CENTER = 'communication_center';
    public const COMBUSTION_CHAMBER = 'combustion_chamber';
    public const HEART_OF_NERON = 'heart_of_neron';
    public const KITCHEN = 'kitchen';
    public const NARCOTIC_DISTILLER = 'narcotic_distiller';
    public const SHOWER = 'shower';
    public const SUPPORT_DRONE = 'support_drone';
    public const DYNARCADE = 'dynarcade';
    public const JUKEBOX = 'jukebox';
    public const RESEARCH_LABORATORY = 'research_laboratory';
    public const BED = 'bed';
    public const COFFEE_MACHINE = 'coffee_machine';
    public const CRYO_MODULE = 'cryo_module';
    public const MYOSCAN = 'myoscan';
    public const PILGRED = 'pilgred';
    public const SHOOTING_STATION = 'shooting_station';
    public const SURGICAL_PLOT = 'surgical_plot';
    public const EMERGENCY_REACTOR = 'emergency_reactor';
    public const REACTOR_LATERAL_ALPHA = 'reactor_lateral_alpha';
    public const REACTOR_LATERAL_BRAVO = 'reactor_lateral_bravo';
    public const FUEL_TANK = 'fuel_tank';
    public const OXYGEN_TANK = 'oxygen_tank';
    public const PLANET_SCANNER = 'planet_scanner';
    public const GRAVITY_SIMULATOR = 'gravity_simulator';
    public const ASTRO_TERMINAL = 'astro_terminal';
    public const COMMAND_TERMINAL = 'command_terminal';
    public const THALASSO = 'thalasso';
    public const BIOS_TERMINAL = 'bios_terminal';
}
