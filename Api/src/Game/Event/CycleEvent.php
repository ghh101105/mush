<?php

namespace Mush\Game\Event;

class CycleEvent extends AbstractTimeEvent
{
    public const NEW_CYCLE = 'new.cycle';
}
