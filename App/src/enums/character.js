const ANDIE = "andie";
const CHAO = "chao";
const CHUN = "chun";
const DEREK = "derek";
const ELEESHA = "eleesha";
const FINOLA = "finola";
const FRIEDA = "frieda";
const GIOELE = "gioele";
const HUA = "hua";
const IAN = "ian";
const JANICE = "janice";
const JIN_SU = "kim_jun_su";
const KUAN_TI = "kuan_ti";
const PAOLA = "paola";
const RALUCA = "raluca";
const ROLAND = "roland";
const STEPHEN = "stephen";
const TERRENCE = "terrence";


export const characterEnum = {
    [ANDIE]: {
        'body': require('@/assets/images/char/body/andie.png'),
        'portrait': require('@/assets/images/char/portrait/andie_graham_portrait.jpg')
    },
    [CHAO]: {
        'body': require('@/assets/images/char/body/chao.png'),
        'portrait': require('@/assets/images/char/portrait/Wang_chao_portrait.jpg')
    },
    [CHUN]: {
        'body': require('@/assets/images/char/body/chun.png'),
        'portrait': require('@/assets/images/char/portrait/Zhong_chun_portrait.jpg')
    },
    [DEREK]: {
        'body': require('@/assets/images/char/body/derek.png'),
        'portrait': require('@/assets/images/char/portrait/derek_hogan_portrait.jpg')
    },
    [ELEESHA]: {
        'body': require('@/assets/images/char/body/eleesha.png'),
        'portrait': require('@/assets/images/char/portrait/Eleesha_williams_portrait.jpg')
    },
    [FINOLA]: {
        'body': require('@/assets/images/char/body/finola.png'),
        'portrait': require('@/assets/images/char/portrait/Finola_keegan_portrait.jpg')
    },
    [FRIEDA]: {
        'body': require('@/assets/images/char/body/frieda.png'),
        'portrait': require('@/assets/images/char/portrait/Frieda_bergmann_portrait.jpg')
    },
    [GIOELE]: {
        'body': require('@/assets/images/char/body/gioele.png'),
        'portrait': require('@/assets/images/char/portrait/Gioele_rinaldo_portrait.jpg')
    },
    [HUA]: {
        'body': require('@/assets/images/char/body/hua.png'),
        'portrait': require('@/assets/images/char/portrait/Jiang_hua_portrait.jpg')
    },
    [IAN]: {
        'body': require('@/assets/images/char/body/ian.png'),
        'portrait': require('@/assets/images/char/portrait/Ian_soulton_portrait.jpg')
    },
    [JANICE]: {
        'body': require('@/assets/images/char/body/janice.png'),
        'portrait': require('@/assets/images/char/portrait/Janice_kent_portrait.jpg')
    },
    [JIN_SU]: {
        'body': require('@/assets/images/char/body/jin_su.png'),
        'portrait': require('@/assets/images/char/portrait/Kim_jin_su_portrait.jpg')
    },
    [KUAN_TI]: {
        'body': require('@/assets/images/char/body/kuan_ti.png'),
        'portrait': require('@/assets/images/char/portrait/Lai_kuan_ti_portrait.jpg')
    },
    [PAOLA]: {
        'body': require('@/assets/images/char/body/paola.png'),
        'portrait': require('@/assets/images/char/portrait/Paola_rinaldo_portrait.jpg')
    },
    [RALUCA]: {
        'body': require('@/assets/images/char/body/raluca.png'),
        'portrait': require('@/assets/images/char/portrait/Raluca_tomescu_portrait.jpg')
    },
    [ROLAND]: {
        'body': require('@/assets/images/char/body/roland.png'),
        'portrait': require('@/assets/images/char/portrait/Roland_zuccali_portrait.jpg')
    },
    [STEPHEN]: {
        'body': require('@/assets/images/char/body/stephen.png'),
        'portrait': require('@/assets/images/char/portrait/Stephen_seagull_portrait.jpg')
    },
    [TERRENCE]: {
        'body': require('@/assets/images/char/body/terrence.png'),
        'portrait': require('@/assets/images/char/portrait/Terrence_archer_portrait.jpg')
    },
}