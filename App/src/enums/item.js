const COFFEE = "coffee";
const PLASTIC_SCRAP = "plastic_scraps";
const METAL_SCRAP = "metal_scraps";
const BACTA = "bacta";
const CREEPNUT = "creepnut";
const SNIPER_HELMET_BLUEPRINT = "sniper_helmet_blueprint";
const SNIPER_HELMET = "sniper_helmet";
const APPRENTON_PILOTE = "apprenton_pilot";


export const itemEnum = {
    [COFFEE]: {
        'image': require('@/assets/images/items/coffee.jpg')
    },
    [METAL_SCRAP]: {
        'image': require('@/assets/images/items/metal_scraps.jpg')
    },
    [PLASTIC_SCRAP]: {
        'image': require('@/assets/images/items/plastic_scraps.jpg')
    },
    [APPRENTON_PILOTE]: {
        'image': require('@/assets/images/items/book.jpg')
    },
    [BACTA]: {
        'image': require('@/assets/images/items/drug_8.jpg')
    },
    [CREEPNUT]: {
        'image': require('@/assets/images/items/fruit01.jpg')
    },
    [SNIPER_HELMET_BLUEPRINT]: {
        'image': require('@/assets/images/items/blueprint.jpg')
    },
    [SNIPER_HELMET]: {
        'image': require('@/assets/images/items/aiming_helmet.jpg')
    },
}